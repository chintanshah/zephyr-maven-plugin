package com.thed.zapi.rest;

import com.thed.zapi.rest.config.Configuration;
import com.thed.zapi.rest.vo.StatusVO;
import org.codehaus.jettison.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by smangal on 1/30/14.
 */
public class ZephyrRestHelperTest {
    private Configuration config;
    private ZapiRestHelper impl;
    private String fieldVal;

    @Before
    public void setUp(){
        config = new Configuration("http://localhost:2990/jira/", "admin", "admin", 1, "10000", "10000");
        impl = new ZapiRestHelper(config);
        fieldVal = "com.thed.test.ZephyrRestHelperTest" + System.currentTimeMillis();
    }

    @Test
    public void createCycle() throws JSONException {
        // Please, do not remove this line from file template, here invocation of web service will be inserted
        AbstractCollection<Long> cycles = new ArrayList<Long>(100);
        for (int i = 0; i < 1; i++) {
            Long cycleId = impl.createCycle("UTCycle");
            assertNotNull(cycleId);
            cycles.add(cycleId);
        }

        Iterator<Long> it = cycles.iterator();
        while (it.hasNext()) {
            impl.deleteCycle(it.next());
        }
    }

    @Test
    public void getExecutionStatus(){
        List<StatusVO> statuses = impl.getExecutionStatuses();
        assertNotNull(statuses);
        assertTrue(statuses.size() > 0);

        List<StatusVO> stepStatuses = impl.getStepStatuses();
        assertNotNull(stepStatuses);
        assertTrue(stepStatuses.size() > 0);
    }

    @Test
    public void executeTest()  throws JSONException, IOException{
        Long expectedTestcaseId = impl.findOrAddTestcase("javaclass", fieldVal);
        Long cycleId = impl.createCycle("UTCycle");
        impl.executeTest(cycleId.intValue(), expectedTestcaseId.intValue(), "2", "Fake Comments");
        impl.deleteCycle(cycleId);
    }

    @Test
    public void findOrAddTestcase() throws JSONException, IOException {
        Long expectedTestcaseId = impl.findOrAddTestcase("javaclass", fieldVal);
        assertNotNull(expectedTestcaseId);
        assertTrue(expectedTestcaseId > 0);
    }

}
